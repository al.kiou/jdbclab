/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DB {
    
    private String     db = null;
    private String     driverClassName = "oracle.jdbc.OracleDriver" ;
    private String     url = "jdbc:oracle:thin:@192.168.6.21:1521:dblabs" ;
    private String     username = "it164683";
    private String     passwd = "bill210_koukoS2";
    private Connection dbConnection = null;
    private Statement  statement = null;
    private ResultSet  rs = null;

    public DB(String db) throws Exception {
        if (!db.equals("oracle") && !db.equals("postgres"))
            throw new Exception("DB must be either 'oracle' or 'postgress");
        this.db = db;
        
    }
    
    // connect to DB, sets dbConnection 
    public void connect(){
        switch (db){
            case "oracle":
                driverClassName = "oracle.jdbc.OracleDriver" ;
                url = "jdbc:oracle:thin:@192.168.6.21:1521:dblabs" ;
                username = "it164683";
                passwd = "bill210_koukoS2";
                break;
            case "postgres":
                driverClassName = "org.postgresql.Driver" ;
                url = "jdbc:postgresql://dblabs.it.teithe.gr:5432/it164683" ;
                username = "it164683";
                passwd = "bill210_koukoS2";
                break;
        }// switch        
                
        try {
            Class.forName (driverClassName); // sets Class according to the DB Manager selected
        } catch (ClassNotFoundException ex) {
            System.out.println("Make sure that the .jar file is placed in libraries or the \\Java\\jdk1.8.0_111\\jre\\lib\\ext folder");
            System.out.println(ex);
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            dbConnection = DriverManager.getConnection (url, username, passwd); // initiates connection
        } catch (SQLException ex) {
            System.out.println("Connection Failed");
            System.out.println(ex);
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // terminate connection to DB
    public void close(){
        try {
            dbConnection.close(); // self explanatory
        } catch (SQLException ex) {
            System.out.println("Closing connection failed");
            System.out.println(ex);
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void executeUpdate(String query){
        try {
            System.out.println("Executing: " +query);
            statement = dbConnection.createStatement(); // create statement
            statement.executeUpdate(query); // execute query
            statement.close(); // close statement
        } catch (SQLException ex) {
            System.out.println("Update Execution failed");
            System.out.println(ex);
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // returns result set of given query
    public ResultSet resultSet(String query){
        try {
            System.out.println("Searching: " +query);
            statement = dbConnection.createStatement(); // create statement
            rs = statement.executeQuery(query); // retrieve result set from DB
            return rs; // return result set
        } catch (SQLException ex) {
            System.out.println("Search Execution failed");
            System.out.println(ex);
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null; // if Exception occurs
    }
    
    public String resultSetString(String query){
        String s = ""; // to be returned
        try {
            System.out.println("Searching: " +query);
            statement = dbConnection.createStatement(); // create statement
            rs = statement.executeQuery(query); // retreive result set
            ResultSetMetaData rsmd = rs.getMetaData(); // get metadata from result set
            int columnsNumber = rsmd.getColumnCount(); // get column number
            while (rs.next()) { // for every row
                for (int i = 1; i <= columnsNumber; i++) { // for every column, 
                    // add to string column name and value
                    if (i > 1) s +=(",  ");
                    String columnValue = rs.getString(i);
                    s += (rsmd.getColumnName(i) + ": " + columnValue );
                }// for
                s+= "\n";
            }// while
        }catch (SQLException ex) {
            System.out.println("Search Execution failed");
            System.out.println(ex);
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }// catch
        return s;
    }
}

