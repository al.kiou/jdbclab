/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author vas_k
 */
public class JDBC {

    public static void main (String[] argv) throws Exception
    {
        DB db = new DB("oracle");
        db.connect();
        db.executeUpdate("DROP TABLE Sailors");
        db.executeUpdate("CREATE TABLE Sailors(" +
					  "sid integer not null primary key," +
					  "sname varchar(15)," +
					  "rating integer," +
					  "age integer)");
        db.executeUpdate("insert into Sailors (sid, sname, rating, age) values (22, 'Dustin', 7, 45)");
        System.out.print(db.resultSetString("Select * from Sailors"));
//        while(rs.next()) {
//		String name = rs.getString("sname");
//		System.out.println( name );
//	}
        db.close();
    }
}